<?php

use Slim\Http\Request;
use Slim\Http\Response;

// Routes

$app->get('/', function (Request $request, Response $response, array $args)
{
    $this->logger->info("zurich-ar-server GET '/'");

    // Render
    return $this->renderer->render($response, 'index.phtml', $args);
});

$app->get('/user', function (Request $request, Response $response, array $args)
{
    $this->logger->info("zurich-ar-server GET '/user'");
	
    $query = $this->db->prepare("SELECT id FROM usuarios WHERE mail = '" . $request->getParam('mail') . "' AND dni = '" . $request->getParam('dni') . "' LIMIT 1");
    $query->execute();
    $result = $query->fetchAll();

	if (count($result) > 0)
	{
		$id = $result[0]["id"];
		$query = $this->db->prepare("SELECT COUNT(id) AS pregunta_actual FROM respuestas WHERE user_id = " . $id);
		$query->execute();
		$result = $query->fetchAll();
		
		$result[0]["user_id"] = $id;
		$response->getBody()->write(json_encode($result[0]));
		
		return $response->withStatus(200);
	}
	else {
		if(filter_var($request->getParam('mail'), FILTER_VALIDATE_EMAIL))
		{
			if (end(explode('@', $request->getParam('mail'))) == "zurich.com")
			{
				$query = $this->db->prepare("INSERT INTO usuarios (mail, dni) VALUES (:userMail, :userDNI);");
				$query->bindParam('userMail', $request->getParam('mail'));
				$query->bindParam('userDNI', $request->getParam('dni'));
				$query->execute();
				
				$query = $this->db->prepare("SELECT LAST_INSERT_ID() AS user_id FROM usuarios");
				$query->bindParam('id', $args['id']);
				$query->execute();
				$result = $query->fetch();
				
				$result['pregunta_actual'] = 0;
				$response->getBody()->write(json_encode($result));
				return $response->withStatus(200);
			}
		}
	}
	
	return $response->withStatus(401);
});

$app->get('/correctas', function (Request $request, Response $response, array $args)
{
    $this->logger->info("zurich-ar-server GET '/correctas'");
	
	$query = $this->db->prepare("SELECT COUNT(*) AS correctas FROM respuestas WHERE question >= " . $request->getParam('pregunta_id') . " AND user_id = " . $request->getParam('user_id') . " AND correct = 1");
	$query->execute();
	$result = $query->fetch();
	$result['ganador'] = false;
	
	$query = $this->db->prepare("SELECT COUNT(*) AS total FROM respuestas WHERE user_id = " . $request->getParam('user_id') . " AND correct = 1");
	$query->execute();
	if ($query->fetch()['total'] == 15)
	{
		$query = $this->db->prepare("SELECT COUNT(*) AS ganadores FROM ganadores WHERE DATE(timestamp) = CURDATE()");
		$query->execute();
		if ($query->fetch()['ganadores'] < 60)
		{
			$result['ganador'] = true;
				
			$query = $this->db->prepare("INSERT IGNORE INTO ganadores (user_id) VALUES (:userID)");
			$query->bindParam('userID', $request->getParam('user_id'));
			$query->execute();
		}
	}
	
	$response->getBody()->write(json_encode($result));
	
	return $response->withStatus(200);
});

$app->post('/answer', function (Request $request, Response $response, array $args)
{
    $this->logger->info("zurich-ar-server POST '/answer'");
    //$requestBody = json_decode($request->getBody()->getContents());
    $requestBody = $request->getParsedBody();
    
    $query = $this->db->prepare("INSERT INTO respuestas (user_id, question, answer, correct) VALUES (:userID, :question, :answer, :correct)");
    //$query->bindParam('userID', $requestBody->user_id);
    //$query->bindParam('question', $requestBody->question);
    //$query->bindParam('answer', $requestBody->answer);
    //$query->bindParam('correct', $requestBody->correct);
    $query->bindParam('userID', $requestBody['user_id']);
    $query->bindParam('question', $requestBody['question']);
    $query->bindParam('answer', $requestBody['answer']);
    $query->bindParam('correct', $requestBody['correct']);
    $query->execute();
    
    return $response->withStatus(200);
});

$app->post('/points', function (Request $request, Response $response, array $args)
{
    $this->logger->info("zurich-ar-server POST '/points'");
    //$requestBody = json_decode($request->getBody()->getContents());
    $requestBody = $request->getParsedBody();
    
    $query = $this->db->prepare("UPDATE usuarios SET points = points + :points WHERE id = :userID");
    //$query->bindParam('userID', $requestBody->user_id);
    //$query->bindParam('points', $requestBody->points);
    $query->bindParam('userID', $requestBody['user_id']);
    $query->bindParam('points', $requestBody['points']);
    $query->execute();
	
    return $response->withStatus(200);
});

$app->post('/timeout', function (Request $request, Response $response, array $args)
{
    $this->logger->info("zurich-ar-server POST '/timeout'");
	$requestBody = $request->getParsedBody();
	
	$query = $this->db->prepare("INSERT INTO respuestas (user_id, question, answer, correct) VALUES (:userID, :question, '-1', '0')");
	$query->bindParam('userID', $requestBody['user_id']);
	$query->bindParam('question', $requestBody['question']);
	$query->execute();
	
	//$current = $requestBody['question'];
	//$target = $current - ($current % 5) + 5;
	
	//for ($i = $current; $i < $target; ++$i)
	//{
	//	$query = $this->db->prepare("INSERT INTO respuestas (user_id, question, answer, correct) VALUES (:userID, :question, '-1', '0')");
	//	$query->bindParam('userID', $requestBody['user_id']);
	//	$query->bindParam('question', $i);
	//	$query->execute();
	//}
	
    return $response->withStatus(200);
});